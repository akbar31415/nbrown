package co.uk.nbrown;

import java.util.List;

public class Contenders {

    private List<Contender> contenders;

    public Contenders(List<Contender> contenders) {
        this.contenders = contenders;
    }

    public List<Contender> getContenders() {
        return contenders;
    }
}

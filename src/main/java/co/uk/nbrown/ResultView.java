package co.uk.nbrown;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Logger.getLogger;

public class ResultView {
    private Logger LOGGER = getLogger(ResultView.class.getName());
    private ObjectMapper mapper = new ObjectMapper();
    public String getJson(Contenders contenders) {
        String result="";
        try {
            result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contenders);
        } catch (JsonProcessingException e) {
            LOGGER.log(Level.SEVERE,e.getMessage());
        }
        return result;
    }
}

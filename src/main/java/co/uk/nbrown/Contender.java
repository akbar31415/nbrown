package co.uk.nbrown;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"name", "points"})
public interface Contender {
    void setContender(String name, int points);
}

package co.uk.nbrown;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.logging.Logger;

@Controller
public class WebController {
    private MainPage mainPage=new MainPage("https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/");
    private ResultView  resultView=new ResultView();
    private Logger LOGGER = Logger.getLogger(WebController.class.getName());

    public String getResult(String clazz,int numberOfResults,int... columns) {
        String[][] table = mainPage.getTableByClassResult(clazz,numberOfResults);
        Contenders contenders=new Contenders(FilterColumn.filterForColumns(Driver.class,table,columns));
        return resultView.getJson(contenders);
    }

    @GetMapping("/drivers")
    @ResponseBody
    public String getDriverJson(){
        return getResult("motor-sport-results msr_season_driver_results", 10,1,23).replace("contenders","drivers");
    }
    @GetMapping("/teams")
    @ResponseBody
    public String getTeamJson(){
        return getResult("motor-sport-results msr_season_team_results", 5,1,24).replace("contenders","teams");
    }

    @RequestMapping("/index")
    public ModelAndView mainPage() {
        return new ModelAndView("index", "message", "F1 Results");
    }
}

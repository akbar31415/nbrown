package co.uk.nbrown;

public class Driver extends Points implements Contender{
    private String driverName;

    @Override
    public void setContender(String driverName, int points) {
        this.driverName=driverName;
        this.points=points;
    }

    public String getName() {
        return driverName;
    }
}

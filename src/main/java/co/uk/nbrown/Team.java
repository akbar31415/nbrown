package co.uk.nbrown;

public class Team extends Points implements Contender {
    private String teamName;

    @Override
    public void setContender(String driverName, int points) {
        this.teamName =driverName;
        this.points=points;
    }

    public String getName() {
        return teamName;
    }
}

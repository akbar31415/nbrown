package co.uk.nbrown;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainPage {

    private Document mainPage;
    private Logger LOGGER = Logger.getLogger(MainPage.class.getName());

    public MainPage(String urlString) {
        getPage(urlString);
    }

    private void getPage(String urlString) {
        try {
            mainPage = Jsoup.connect(urlString).get();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE,e.toString());
        }
    }

    public Object getDoc() {
        return mainPage;
    }

    public Elements getElementsByClass(String clazz) {
        return mainPage.getElementsByClass(clazz);
    }

    public String[][] getTableByClass(String clazz) {
        return getTable(getElementsByClass(clazz).get(0));
    }

    private String[][] getTable(Element table){
        Elements tableRows = table.select("tr");
        String[][] tableArray = new String[tableRows.size()][];
        for (int i = 0; i < tableRows.size(); i++) {
            Elements tableColumn = tableRows.get(i).select("td");
            tableArray[i] = new String[tableColumn.size()];
            for (int j = 0; j < tableColumn.size(); j++) {
                tableArray[i][j] = tableColumn.get(j).text();
            }
        }
        return tableArray;
    }

    public String[][] getTableByClassResult(String clazz, int numberOfResults) {
        String[][] table= getTableByClass(clazz);
        String[][] result=new String[numberOfResults][];
        boolean driverTable=clazz.contains("driver");
        for (int i = 1; i <1+numberOfResults ; i++) {
            //skip every even row for team table, each team has two vehicle and only one total point
            result[i-1]=table[driverTable?i:(i*2)-1];
        }
        return result;
    }
}

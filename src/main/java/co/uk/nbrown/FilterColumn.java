package co.uk.nbrown;

import java.util.ArrayList;
import java.util.List;

public class FilterColumn {
    public static List<Contender> filterForColumns(Class<?> clazz, String[][] table, int... j) {
        List<Contender> contenderList = new ArrayList<>();
        for (String[] strings : table) {
            Contender contender = null;
            try {
                contender = (Contender) clazz.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            contender.setContender(strings[j[0]], Integer.valueOf(strings[j[1]]));
            contenderList.add(contender);
        }
        return contenderList;
    }
}

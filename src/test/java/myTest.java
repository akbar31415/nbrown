import co.uk.nbrown.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class myTest {
     /*
    The technical assessment for the senior developer position is to create a simple MVC java application to generate
    formula one race results for the 2018 season from information provided online as HTML.

    The application will have a web interface which will make ajax calls to the server, return JSON format data and
    using JavaScript to format this data onto the page.

    The application will extract grand prix information from the site below and output the drivers and teams results.
    The driver’s results will show the top 10 drivers and the team results will show the top 5 teams. Both results will
    show and be ordered based on the total points scored.

    The following link provides a list of grand prix within the season with links to each race result.

    https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/

    The application must be production ready with sufficient testing in place. Java 8 should be used but you are free
    to use any other technology you wish to help with the implementation. The application must be committed into
    Bitbucket and shared with a technical assessor.

    The build process will need to be based on a common build technology with instructions provided on how to build and
    execute the application from the command line.

    There is no time limit for the assessment but it is expected to take no more than 2 hours.
    */

    MainPage mainPage;
    @Before
    public void set(){
        mainPage=new MainPage("https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/");
    }

    @Test
    public void getLinkContent(){
        assertNotNull(mainPage.getDoc());
    }

    @Test
    public void getFirstTableByClassName(){
        assertNotNull(mainPage.getElementsByClass("motor-sport-results msr_season_driver_results"));
    }

    @Test
    public void getSecondTableByClassName(){
        assertNotNull(mainPage.getElementsByClass("motor-sport-results msr_season_team_results"));
    }

    @Test
    public void getFirstTableDataByClassName(){
        String[][] table = mainPage.getTableByClass("motor-sport-results msr_season_driver_results");
        assertNotNull(table);
    }

    @Test
    public void getSecondTableDataByClassName() {
        String[][] table = mainPage.getTableByClass("motor-sport-results msr_season_team_results");
        assertNotNull(table);
    }

    @Test
    public void get10DriverResults(){
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_driver_results",10);
        assertEquals(table.length,10);
    }

    @Test
    public void get5TeamResults(){
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_team_results",5);
        assertEquals(table.length,5);
    }

    @Test
    public  void getNameAndPointsForDriverInJsonFormat()throws JsonProcessingException {
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_driver_results",10);
        Contenders contenders=new Contenders(FilterColumn.filterForColumns(Driver.class,table,1,23));
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            result=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contenders);
        } catch (JsonProcessingException e) {

        }
        int count = 0;
        Matcher matcher = Pattern.compile("name").matcher(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));
        while (matcher.find()) count++;
        //returns all in json
        Assert.assertEquals(10,count);
    }
    @Test
    public  void getNameAndPointsForTeamInJsonFormat() throws JsonProcessingException {
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_team_results",5);
        Contenders contenders=new Contenders(FilterColumn.filterForColumns(Team.class,table,1,24));
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            result=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contenders);
        } catch (JsonProcessingException e) {

        }
        int count = 0;
        Matcher matcher = Pattern.compile("name").matcher(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));
        while (matcher.find()) count++;
        //returns all in json
        Assert.assertEquals(5,count);
    }

    @Test
    public void testControllerReturnsCorrectJsonForTeam(){
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_team_results",5);
        Contenders contenders=new Contenders(FilterColumn.filterForColumns(Team.class,table,1,24));
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            result=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contenders);
        } catch (JsonProcessingException e) {

        }
        WebController controller=new WebController();
        Assert.assertEquals(result,controller.getResult("motor-sport-results msr_season_team_results", 5,1,24));
    }
    @Test
    public void testControllerReturnsCorrectJsonForDriver(){
        String[][] table = mainPage.getTableByClassResult("motor-sport-results msr_season_driver_results",10);
        Contenders contenders=new Contenders(FilterColumn.filterForColumns(Driver.class,table,1,23));
        ObjectMapper mapper = new ObjectMapper();
        String result = "";
        try {
            result=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contenders);
        } catch (JsonProcessingException e) {

        }
        WebController controller=new WebController();
        Assert.assertEquals(result,controller.getResult("motor-sport-results msr_season_driver_results", 10,1,23));
    }
}
